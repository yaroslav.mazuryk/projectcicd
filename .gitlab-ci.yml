####################################################
# The docker image the jobs initialize with.
# We use nodejs, so a node image makes sense.
# https://docs.gitlab.com/ee/ci/yaml/README.html#image
####################################################
image: "node:latest"
####################################################
# Cache node modules between jobs.
# Downloading node dependencies can take a long time,
# avoiding that cost between jobs/stages is good.
# https://docs.gitlab.com/ee/ci/yaml/README.html#cache
####################################################
cache:
 key: ${COMMIT_REF_NAME}
 paths:
  - node_modules/
  - .sfdx/
####################################################
# The sequential stages of this pipeline.
# Jobs within a stage run in parallel.
# https://docs.gitlab.com/ee/ci/yaml/README.html#stages
####################################################
stages:
  - validate
  - UAT
  - PROD
####################################################
# validate into the  CI sandbox org and run all test classes
####################################################
validate:
    stage: validate
    only:
      refs:
        - merge_requests
      variables:
        - $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'main'
    allow_failure: false
    script:
    - install_jq
    - install_salesforce_cli
    - authenticate  UAT $UAT_AUTH_URL $UAT_AUTH_URL
    - validate_metadata_to_sandbox UAT $UAT_AUTH_URL $UAT_AUTH_URL

####################################################
# validate into the  CI sandbox org and run all test classes
####################################################
source-deploy-uat:
 stage: UAT
 only:
  - main
 allow_failure: false
 environment:
  name: Automatic Deployment to UAT Sandbox
  url: https://login.salesforce.com
 script:
  - install_salesforce_cli
  - install_jq
  - authenticate UAT_SANDBOX $UAT_AUTH_URL
  - deploy_metadata_to_sandbox UAT_SANDBOX $UAT_AUTH_URL


####################################################
# Deploy to Production
####################################################

source-deploy-prod:
 stage: PROD
 only:
  - main
 when: manual
 allow_failure: false
 environment:
  name: DEPLOYMENT
  url: https://login.salesforce.com
 script:
  - install_salesforce_cli
  - install_jq
  - authenticate PROD_SANDBOX $PROD_AUTH_URL
  - deploy_metadata_to_sandbox PROD_SANDBOX $PROD_AUTH_URL

.sfdx_helpers: &sfdx_helpers |
 # Function to install the Salesforce CLI.
 # No arguments.
 function install_salesforce_cli() {
  # Salesforce CLI Environment Variables
  # https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_cli_env_variables.htm
  # By default, the CLI periodically checks for and installs updates.
  # Disable (false) this auto-update check to improve performance of CLI commands.
  export SFDX_AUTOUPDATE_DISABLE=false
  # Set to true if you want to use the generic UNIX keychain instead of the Linux libsecret library or macOS keychain.
  # Specify this variable when using the CLI with ssh or "headless" in a CI environment.
  export SFDX_USE_GENERIC_UNIX_KEYCHAIN=true
  # Specifies the time, in seconds, that the CLI waits for the Lightning Experience custom domain to resolve and become available in a newly-created scratch org.
  # If you get errors about My Domain not configured when you try to use a newly-created scratch org, increase this wait time.
  export SFDX_DOMAIN_RETRY=600
  # For force:package:create, disables automatic updates to the sfdx-project.json file.
  export SFDX_PROJECT_AUTOUPDATE_DISABLE_FOR_PACKAGE_CREATE=true
  # For force:package:version:create, disables automatic updates to the sfdx-project.json file.
  export SFDX_PROJECT_AUTOUPDATE_DISABLE_FOR_PACKAGE_VERSION_CREATE=true
  # Install Salesforce CLI
  mkdir sfdx
  CLIURL=https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz
  wget -qO- $CLIURL | tar xJ -C sfdx --strip-components 1
  "./sfdx/install"
  export PATH=./sfdx/$(pwd):$PATH
  # Output CLI version and plug-in information
  sfdx --version
  sfdx plugins --core
 }
 # Function to install jq json parsing library.
 # No arguments.
 function install_jq() {
   apt update && apt -y install jq
 }
 function validate_metadata_to_sandbox(){
   local sandbox_alias=$1
   local sandboxURL=$2
   echo "****** Start Source Validate to $sandbox_alias..."
   sfdx force:source:deploy -p force-app/ -c -u $sandbox_alias 
   echo "****** Source Validated completed to $sandbox_alias.."
 }

 function deploy_metadata_to_sandbox(){
   local sandbox_alias=$1
   local sandboxURL=$2
   echo "****** Start Source Deployment to $sandbox_alias..."
   sfdx force:source:deploy -p force-app/ -u $sandbox_alias
   echo "****** Deployment completed to $sandbox_alias.."
 }

 function run_test_metadata_to_sandbox(){
   local sandbox_alias=$1
   local sandboxURL=$2
   echo "****** Start Source Deployment to $sandbox_alias..."
   sfdx force:source:deploy -c -p force-app/ -u $sandbox_alias -l RunLocalTests
   echo "****** Deployment completed to $sandbox_alias.."
 }

 function run_test_in_sandbox(){
  local sandbox_alias=$1
  local sandboxURL=$2
  echo "status Start test run to $sandbox_alias..."
  sfdx force:apex:test:run -c -u $sandbox_alias -r human
  echo "status Test run finished on $sandbox_alias..."
 }
 function authenticate() {
  local alias_to_set=$1
  local org_auth_url=$2
  local org_auth_url_backup=$3
  local file=$(mktemp)
  echo "status file: $file"
  echo $org_auth_url > $file
  local cmd="sfdx force:auth:sfdxurl:store --sfdxurlfile $file --setalias $alias_to_set --json" && (echo $cmd >&2)
  local output=$($cmd)
  sfdx force:config:set defaultusername=$alias_to_set
  sfdx force:config:set defaultdevhubusername=$alias_to_set
  echo "status Trying to open an org..."
  rm $file
 }
before_script:
 - *sfdx_helpers